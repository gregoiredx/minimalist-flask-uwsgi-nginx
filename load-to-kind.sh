#!/usr/bin/env bash

set -e
set -x

# kind delete cluster

kind create cluster --config kind-cluster.yaml

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml
kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=180s


docker-compose build
kind load docker-image minimalist-flask-uwsgi-nginx_app
kind load docker-image minimalist-flask-uwsgi-nginx_nginx

kubectl apply -f app/deployment.yaml
kubectl apply -f app/service.yml

kubectl apply -f nginx/deployment.yaml
kubectl apply -f nginx/service.yml

kubectl apply -f ingress.yaml
